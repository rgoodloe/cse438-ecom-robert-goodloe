package com.example.cse438.studio1.activity

import android.app.ActionBar
import android.app.Dialog
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.view.MenuItem
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.Toast
import com.example.cse438.studio1.R
import com.example.cse438.studio1.fragment.HomeFragment
import com.example.cse438.studio1.fragment.ResultListFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    enum class Fragments {
        HOME,
        RESULTS
    }

    private val fm = this.supportFragmentManager
    private var currentView: Fragments = Fragments.HOME

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        showFragment(HomeFragment(this))

        //Examine the search functionality below
        search_edit_text.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                val searchText = search_edit_text.text
                search_edit_text.setText("")
                if (searchText.toString() == "") {
                    val toast = Toast.makeText(this@MainActivity, "Please enter text", Toast.LENGTH_SHORT)
                    toast.setGravity(Gravity.CENTER, 0, 0)
                    toast.show()
                    return@setOnEditorActionListener true
                }
                else {
                    performSearch(searchText.toString())
                    return@setOnEditorActionListener false
                }
            }

            return@setOnEditorActionListener false
        }
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }
    

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.b_home -> {
                if (currentView != Fragments.HOME) {

                    currentView = Fragments.HOME

                    showFragment(HomeFragment(this))
                }
            }
            R.id.b_about_us -> {
                displayDialog(R.layout.dialog_about_us)

            }
            R.id.b_privacy_policy -> {
                displayDialog(R.layout.dialog_privacy_policy)
            }

        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun performSearch(query: String) {

        currentView = Fragments.RESULTS

        showFragment(ResultListFragment(this, query))
    }

    private fun displayDialog(layout: Int) {
        val dialog = Dialog(this)
        dialog.setContentView(layout)

        val window = dialog.window
        window?.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT)

        dialog.findViewById<Button>(R.id.close).setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun showFragment(fragment: Fragment) {

        fm.beginTransaction()
            .addToBackStack(null)
            .replace(R.id.frag_placeholder, fragment)
            .commit()

    }
}
